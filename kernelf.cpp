#include "kernelf.h"
#include "math.h"

#include <QDebug>

/* Kernel function with finite support
 * Using the following function from a SPH paper by Monaghan (1992)
 * (Information taken from "Foundations of Physically Based Modeling and Animation" p.289)
 *
 * r = r(v) = |v|
 * w(r) = 1/(pi*s^3) * {    1 - (3/2)(r/s)^2 + (3/4)(r/s)^3 if 0 <= r/s <= 1
 *                          (1/4)(2 - r/s)^3                if 1 <= r/s <= 2
 *                          0                               else
 */

KernelF::KernelF(){
    _s = 1.0f;
}

KernelF::KernelF(float supportRad)
{
    _s = supportRad/2.0f;

    //for(float t = 0; t < 2.1; t+=0.01){
    //    getGradientPart(t);
    //}
}

float KernelF::getSupportRad()
{
    return 2.0f*_s;
}

// w(r(v))
float KernelF::getVal(QVector3D v)
{
    float rs = v.length()/_s;
    if (rs > 2){
        return 0;
    }
    float val = 1.0/(M_PI*pow(_s,3));
    if(rs < 1){
        return val*(1.0f - (3.0f/2.0f)*pow(rs,2) + (3.0f/4.0f)*pow(rs,3));
    }
    else{
        return val*(1.0f/4.0f)*pow(2.0f - rs,3);
    }
}

// grad[w(r)](v) = grad[r](v) * dw/dr(r(v))
// grad[r](v) = v/|v|
QVector3D KernelF::getGradient(QVector3D v)
{
    return v*getGradientPart(v.length());
}

// lap[w(r)](v) = (2/|v|)*(dw/dr)(r(v)) + (dw/dr)^2(r(v))
float KernelF::getLaplacian(QVector3D v)
{
    float rs = v.length()/_s;
    if (rs > 2){
        return 0.0f;
    }

    float val = 1.0/(M_PI*pow(_s,5));
    if(rs < 1){
        val *= 3.0*(-1.0 + (3.0/2.0)*rs);
    }
    else{
        val *= (3.0/2.0)*(2.0 - rs);
    }

    return 2.0f*getGradientPart(v.length()) + val;
}

/* The full gradient formula is:
   v * (1/|v| * dw/dr(r(v)))
  This function just returns the (1/|v| * dw/dr(r(v))) part
   which is also a component of the laplacian calculation
*/

float KernelF::getGradientPart(float vLen)
{
    // rs = |v|/s
    float rs = vLen/_s;
    if (rs > 2){
        return 0.0f;
    }

    float val = 1.0f/(M_PI*pow(_s,4));
    if(rs < 1){
        //val *= 3.0*rs*(-1.0 + (3.0/4.0)*rs) / vLen;
        val *= (3.0f/_s)*(-1.0f + (3.0f/4.0f)*rs);
    }
    else{
        val *= -(3.0f/4.0f)*pow(2.0f - rs,2) / vLen;
    }

    //qDebug() << rs << " -> " << val;//*vLen;
    return val;
}
