#include "fpsaverager.h"

FpsAverager::FpsAverager(int sampleCount)
{
    _size = sampleCount;
    _samples = new float[_size];
    _idx = 0;
    for(int i=0; i < _size; i++){
        _samples[i] = 0.0f;
    }
}

void FpsAverager::reportValue(float val)
{
    _samples[_idx] = val;
    _idx = (_idx+1)%_size;
}

float FpsAverager::getValue()
{
    float sum = 0;
    for(int i=0; i<_size; i++){
        sum += _samples[i];
    }
    return sum / float(_size);
}
