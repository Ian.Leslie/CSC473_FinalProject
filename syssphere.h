#ifndef SYSSPHERE_H
#define SYSSPHERE_H

#include "sysparticle.h"

class SysSphere : public SysParticle
{
public:
    SysSphere();
    SysSphere(float rad, float mass, QVector3D pos, rp3d::DynamicsWorld* worldRef);

    void render();

    float rad;
};

#endif // SYSSPHERE_H
