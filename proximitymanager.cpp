#include "proximitymanager.h"
#include <QVector3D>
#include <math.h>
#include <QDebug>
#include <assert.h>

ProximityManager::ProximityManager()
{
    _smallestGroup = 5;

    _inactiveList = QList<SysParticle*>();
    _activeList = QList<SysParticle*>();
    _kdTree = nullptr;
    _bothTypes = false;
}

ProximityManager::ProximityManager(bool bothTypes)
{
    _smallestGroup = 5;

    _inactiveList = QList<SysParticle*>();
    _activeList = QList<SysParticle*>();
    _kdTree = nullptr;
    _bothTypes = bothTypes;
}

ProximityManager::~ProximityManager()
{
    if(_kdTree != nullptr){
        delete _kdTree;
    }
}

void ProximityManager::addObj(SysParticle *objRef)
{
    if(objRef->isRigid && !_bothTypes){
        _inactiveList.append(objRef);
    }
    else{
        _activeList.append(objRef);
    }
}

void ProximityManager::update()
{
    //qDebug() << "Update";

    if(!_bothTypes){
        // Mini optimization, store the _inactiveList length early
        // so that objects added from the active-list dont get re-checked
        // in the second loop
        int oldSize = _inactiveList.length();

        // Remove objects that have become deactivated
        for(int i=_activeList.length()-1; i>=0; i--){
            if(_activeList[i]->isRigid){
                _inactiveList.append(_activeList[i]);
                _activeList.removeAt(i);
            }
        }

        // Add objects that have become activated
        for(int i=oldSize-1; i>=0; i--){
            if(!_inactiveList[i]->isRigid){
                _activeList.append(_inactiveList[i]);
                _inactiveList.removeAt(i);
            }
        }
    }

    _tempList = new SysParticle*[_activeList.length()];
    if(_kdTree != nullptr){
        delete _kdTree;
    }
    _kdTree = buildKDFor(0,_activeList.length()-1,0);
    delete _tempList;
}

QList<SysParticle *> ProximityManager::getObjsCloseTo(QVector3D pos, float dist)
{
    //qDebug() << "getObjsCloseTo " << pos << ", " << dist;
    // Get all objects in the sphere's bounding box
    QList<SysParticle *> outList = getObjsInZone(pos - QVector3D(dist,dist,dist),pos + QVector3D(dist,dist,dist));
    // Remove objects that are out of range
    for(int i=outList.length()-1; i>=0; i--){
        if((outList[i]->getPosition()-pos).lengthSquared() > dist*dist){
            outList.removeAt(i);
        }
    }

    /*
    bool isCorrect = true;
    //qDebug() << "-----------";
    QList<SysParticle *> outList2 = QList<SysParticle*>();
    for(int i=0; i<_activeList.length(); i++){
        if((_activeList[i]->getPosition()-pos).lengthSquared() <= dist*dist){
            outList2.append(_activeList[i]);
            if(outList.indexOf(_activeList[i]) == -1){
                qDebug() << "MISSING OBJECT: " << _activeList[i]->getPosition() << " Idx=" << i;
                isCorrect=false;
            }
        }
    }
    //qDebug() << "-----------";
    assert(isCorrect);*/

    return outList;
}

QList<SysParticle *> ProximityManager::getObjsInZone(QVector3D minVals, QVector3D maxVals)
{
    //qDebug() << "getObjsInZone " << minVals << ", " << maxVals;
    QList<SysParticle *> outList = QList<SysParticle*>();
    _internalGetObjsInZone(_kdTree,minVals,maxVals,&outList);
    return outList;
}

int ProximityManager::getActiveCount()
{
    return _activeList.length();
}

KDNode* ProximityManager::buildKDFor(int firstIdx, int lastIdx, int dimIdx)
{
    KDNode* node = new KDNode(firstIdx,lastIdx);

    //qDebug() << "buildKDFor" << firstIdx << ", " << lastIdx << ", " << dimIdx;
    int itemCount = lastIdx - firstIdx + 1;
    if(itemCount <= _smallestGroup){
        // If this zone falls below a certain threshold, keep it unordered
        return node;
    }
    sortRegion(firstIdx,lastIdx,dimIdx);
    /*
    qDebug() << "------";
    for(int i=firstIdx; i<=lastIdx; i++){
        qDebug() << _activeList[i]->getPosition().x();
    }
    qDebug() << "------";
    */

    int splitIdx = firstIdx + itemCount/2;
    node->splitDim = dimIdx;
    node->splitValue = _getV(splitIdx,dimIdx);

    //Note: If itemCount is odd, the second group has 1 more element
    node->leftChild = buildKDFor(firstIdx,splitIdx-1,(dimIdx+1)%3);
    node->rightChild = buildKDFor(splitIdx,lastIdx   ,(dimIdx+1)%3);

    return node;
}

// Sorted in ascending order using merge-sort
void ProximityManager::sortRegion(int firstIdx, int lastIdx, int dimIdx)
{
    int itemCount = lastIdx - firstIdx + 1;
    for(int windowSize=1; windowSize<itemCount; windowSize*=2){
        for(int i=0; i<itemCount; i+=windowSize*2){
            int lastSubIdx = firstIdx + i + windowSize*2 - 1;
            if(lastSubIdx > lastIdx){
                lastSubIdx = lastIdx;
            }
            mergedSortedRegions(firstIdx + i,windowSize,lastSubIdx,dimIdx);
        }
    }
}

inline void ProximityManager::mergedSortedRegions(int firstIdx, int windowSize, int maxIdx, int dimIdx)
{
    //qDebug() << "mergedSortedRegions " << firstIdx << ", " << windowSize << ", " << maxIdx << ", " << dimIdx;
    int i = firstIdx;
    int j = firstIdx + windowSize;
    // Write merged & sorted list to the temporary array to avoid having
    // to shuffle around items
    for(int z = firstIdx; z<=maxIdx; z++){
        if(j > maxIdx || (i < firstIdx + windowSize && _isLT(i,j,dimIdx))){
            _tempList[z] = _activeList[i];
            i += 1;
        }
        else{
            _tempList[z] = _activeList[j];
            j += 1;
        }
    }
    // Copy from temporary array back to real region
    for(int z = firstIdx; z <= maxIdx; z++){
        _activeList[z] = _tempList[z];
    }
}

inline bool ProximityManager::_isLT(int i, int j, int dimIdx)
{
    return _getV(i,dimIdx) < _getV(j,dimIdx);
}

inline float ProximityManager::_getV(int i, int dimIdx)
{
    return _activeList[i]->getPosition()[dimIdx];
}

void ProximityManager::_internalGetObjsInZone(KDNode* node, QVector3D minVals, QVector3D maxVals, QList<SysParticle *> *outList)
{
    //qDebug() << "_internalGetObjsInZone ";
    if(node->splitDim == -1){
        // Node has no children
        for(int i = node->startIdx; i<=node->endIdx; i++){
            //qDebug() << "i=" << i << " length=" << _activeList.length();
            QVector3D pos = _activeList[i]->getPosition();
            bool isInside = true;
            for(int j=0; j<3; j++){
                if(minVals[j] > pos[j] || maxVals[j] <= pos[j]){
                    isInside = false;
                    break;
                }
            }
            if(isInside){
                outList->append(_activeList[i]);
            }
        }
        //qDebug() << "returning";
        return;
    }

    if(minVals[node->splitDim] <= node->splitValue){
        // Look in low group
        _internalGetObjsInZone(node->leftChild,minVals,maxVals,outList);
    }
    if(maxVals[node->splitDim] >= node->splitValue){
        // Look in high group
        _internalGetObjsInZone(node->rightChild,minVals,maxVals,outList);
    }

}
