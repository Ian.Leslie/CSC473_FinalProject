#ifndef FPSAVERAGER_H
#define FPSAVERAGER_H


class FpsAverager
{
public:
    FpsAverager(int sampleCount);

    void reportValue(float val);
    float getValue();

private:
    int _idx;
    int _size;
    float* _samples;
};

#endif // FPSAVERAGER_H
