#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QGLWidget>
#include <QMap>
#include <QList>
#include <QTimer>
#include "glcamera.h"
#include "GL/gl.h"
#include "reactphysics3d.h"
#include "sysparticle.h"
#include "proximitymanager.h"
#include "kernelf.h"
#include <QLabel>
#include <QCheckBox>
#include <QSlider>
#include "fpsaverager.h"

class GLWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit GLWidget(QWidget *parent = 0);

    void initializeGL();
    void paintGL();
    void resizeGL(int w, int h);

    void setUpGeneralScene(int iMax, int jMax, int kMax, float fractFluid);

    glcamera camera;

    QLabel* fpsLabel;
    QLabel* rigidLabel;
    QLabel* fluidLabel;
    QCheckBox* dynamicCB;
    QCheckBox* colorCodeCB;
    QSlider* thresholdSlider;


private:
    QMap<int,bool> _keyDownState;
    QTimer* _keyCheckTimer;
    QTimer* _animStepTimer;
    bool _animPaused;
    FpsAverager* _fpsAverager;

    rp3d::DynamicsWorld *_dynWorld;
    rp3d::RigidBody *_floor;

    ProximityManager* _proxManagerAll;
    KernelF _scoreKFun;
    int _updateScoreEvery;
    int _updateScoreIn;

    qint64 _lastTimeReport;
    bool _printTime;

    QList<SysParticle*> _partList;
    ProximityManager* _proxManager;
    KernelF _kFun;
    float _fluidStiffness;
    float _fluidBaseDensity;
    float _fluidViscosity;

    void updateObjectStates(float threshold);

    float getRandFloat(float min, float max);
    void reportTime(char* str, qint64 elapsed);

    bool isKeyDown(int keyCode);
    void drawAnimObjects();

private slots:
    void updateCamera();
    void animStep();

protected:
    bool eventFilter(QObject *o, QEvent *e);

signals:

public slots:
    void pauseTimer();
    void resumeTimer();

    void setUpScene1();
    void setUpScene2();
    void setUpScene3();
    void reset();

};

#endif // GLWIDGET_H
