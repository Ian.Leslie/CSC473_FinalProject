#include "syssphere.h"
#include <GL/gl.h>
#include "glutility.h"

SysSphere::SysSphere()
{
}

SysSphere::SysSphere(float rad, float mass, QVector3D pos, rp3d::DynamicsWorld* worldRef){
    this->rad = rad;
    init(mass,pos,worldRef);

    bodyRef->addCollisionShape(new rp3d::SphereShape(rad/SysParticle::PHY_WORLD_SCALE), rp3d::Transform(), mass);

    bodyRef->getMaterial().setBounciness(0.4);
    bodyRef->getMaterial().setFrictionCoefficient(0.2);
    bodyRef->getMaterial().setRollingResistance(0.2);
    bodyRef->setLinearDamping(0.1);

    makeRigid();
}

void SysSphere::render()
{
    glPushMatrix();
    GLUtility::setPositionAndOrientation(getPosition(),getOrientation());
    GLUtility::renderSphere(rad);
    glPopMatrix();
}
