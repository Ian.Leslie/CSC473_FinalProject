#include "glutility.h"
#include <QVector3D>
#include <QtMath>
#include <QDebug>
#include <GL/gl.h>
#include <QQuaternion>
#include <QMatrix4x4>

/* Simple static class that holds some OpenGL related functions
 * Includes some functions to render basic shapes and some
 * wrapper function that let you specify OpenGL vertices using
 * the QVector3D class
 */

void GLUtility::setPositionAndOrientation(QVector3D pos, QQuaternion orient)
{
    glTranslatef(pos.x(),pos.y(),pos.z());
    glMultMatrixf(QMatrix4x4(orient.toRotationMatrix()).constData());
}

void GLUtility::renderBox(float scale){
    renderBox(scale,scale,scale);
}

void GLUtility::renderBox(float xScale, float yScale, float zScale)
{
    QVector3D p[8];
    for(int i=0; i<8; i++){
        p[i] = QVector3D(
                    i<4     ? xScale : -xScale,
                    i%4 < 2 ? yScale : -yScale,
                    i%2==0  ? zScale : -zScale
                    );
    }
    glBegin(GL_QUADS);
    // Face 1
    setNormalFromTriangle(&p[0],&p[1],&p[2]);
    qVertexWrap(&p[0]);
    qVertexWrap(&p[1]);
    qVertexWrap(&p[3]);
    qVertexWrap(&p[2]);
    // Face 2
    setNormalFromTriangle(&p[4],&p[6],&p[7]);
    qVertexWrap(&p[4]);
    qVertexWrap(&p[6]);
    qVertexWrap(&p[7]);
    qVertexWrap(&p[5]);

    // Face 3
    setNormalFromTriangle(&p[1],&p[5],&p[7]);
    qVertexWrap(&p[1]);
    qVertexWrap(&p[5]);
    qVertexWrap(&p[7]);
    qVertexWrap(&p[3]);
    // Face 4
    setNormalFromTriangle(&p[0],&p[2],&p[6]);
    qVertexWrap(&p[0]);
    qVertexWrap(&p[2]);
    qVertexWrap(&p[6]);
    qVertexWrap(&p[4]);

    // Face 5
    setNormalFromTriangle(&p[4],&p[5],&p[1]);
    qVertexWrap(&p[4]);
    qVertexWrap(&p[5]);
    qVertexWrap(&p[1]);
    qVertexWrap(&p[0]);
    // Face 6
    setNormalFromTriangle(&p[3],&p[7],&p[6]);
    qVertexWrap(&p[3]);
    qVertexWrap(&p[7]);
    qVertexWrap(&p[6]);
    qVertexWrap(&p[2]);
    glEnd();
}

void GLUtility::renderSphere(float scale){
    renderSphere(scale,false);
}

void GLUtility::renderSphere(float scale, bool drawLines)
{
    const int VLINE_COUNT = 8;
    const int HSTRIP_COUNT = 5;
    QVector3D points[VLINE_COUNT*HSTRIP_COUNT];
    QVector3D topMid = QVector3D(0,scale,0);
    QVector3D botMid = QVector3D(0,-scale,0);

    // Build array of point data
    for(int i = 0; i < HSTRIP_COUNT; i++){
        float pitch = (float(i+1)/float(HSTRIP_COUNT+1) - 0.5f)*M_PI;
        float yPos = qSin(pitch)*scale;
        float outDist = qCos(pitch)*scale;
        for(int j = 0; j < VLINE_COUNT; j++){
            float yaw = (float(j)/float(VLINE_COUNT)) * 2.0f * M_PI;
            float xPos = qCos(yaw)*outDist;
            float zPos = qSin(yaw)*outDist;
            points[i*VLINE_COUNT + j] = QVector3D(xPos,yPos,zPos);
        }
    }

    // Draw main quads
    QVector3D *quad[4];
    glBegin(GL_QUADS);
    for(int i = 1; i < HSTRIP_COUNT; i++){
        for(int j = 0; j < VLINE_COUNT; j++){
            quad[0] = &points[(i-1)*VLINE_COUNT + (j + 1)%VLINE_COUNT];
            quad[1] = &points[(i-1)*VLINE_COUNT + j];
            quad[2] = &points[(i-0)*VLINE_COUNT + j];
            quad[3] = &points[(i-0)*VLINE_COUNT + (j + 1)%VLINE_COUNT];

            setNormalFromTriangle(quad[2],quad[1],quad[0]);
            for(int k = 0; k < 4; k++){
                qVertexWrap(quad[k]);
            }
        }
    }
    glEnd();

    // Draw top & bottom triangles
    QVector3D *triV[3];
    glBegin(GL_TRIANGLES);
    for(int j = 0; j < VLINE_COUNT; j++){
        triV[0] = &botMid;
        triV[1] = &points[j];
        triV[2] = &points[(j + 1)%VLINE_COUNT];
        setNormalFromTriangle(triV[2],triV[1],triV[0]);
        for(int k = 0; k < 3; k++){
            qVertexWrap(triV[k]);
        }
    }
    for(int j = 0; j < VLINE_COUNT; j++){
        triV[0] = &topMid;
        triV[1] = &points[(HSTRIP_COUNT-1)*VLINE_COUNT + j];
        triV[2] = &points[(HSTRIP_COUNT-1)*VLINE_COUNT + (j + 1)%VLINE_COUNT];
        setNormalFromTriangle(triV[1],triV[2],triV[0]);
        for(int k = 0; k < 3; k++){
            qVertexWrap(triV[k]);
        }
    }
    glEnd();

    if(drawLines){
        glDisable(GL_LIGHTING);
        glColor3f(0,0,0);
        glLineWidth(2.0);

        // Draws horizontal rings
        for(int i = 0; i < HSTRIP_COUNT; i++){
            glBegin(GL_LINE_LOOP);
            for(int j = 0; j < VLINE_COUNT; j++){
                qVertexWrap(&points[i*VLINE_COUNT + j]);
            }
            glEnd();
        }

        // Draws vertical rings
        for(int j = 0; j < VLINE_COUNT; j++){
            glBegin(GL_LINE_STRIP);
            qVertexWrap(&botMid);
            for(int i = 0; i < HSTRIP_COUNT; i++){
                qVertexWrap(&points[i*VLINE_COUNT + j]);
            }
            qVertexWrap(&topMid);
            glEnd();
        }

        glLineWidth(1.0);
        glEnable(GL_LIGHTING);
    }
}

void GLUtility::setNormalFromTriangle(QVector3D *v0, QVector3D *v1, QVector3D *v2)
{
    QVector3D vect1 = *v0 - *v1;
    QVector3D vect2 = *v2 - *v1;
    QVector3D normalV = QVector3D::crossProduct(vect1,vect2).normalized();
    qNormalWrap(&normalV);
}

void GLUtility::qNormalWrap(QVector3D *vect){
    glNormal3f(vect->x(),vect->y(),vect->z());
}

void GLUtility::qVertexWrap(QVector3D *vect){
    glVertex3f(vect->x(),vect->y(),vect->z());
}
