#ifndef SYSPARTICLE_H
#define SYSPARTICLE_H

#include "reactphysics3d.h"
#include <QVector3D>
#include <QQuaternion>

class SysParticle
{
public:
    rp3d::RigidBody* bodyRef;
    bool isRigid;
    float mass;
    float density;
    float preassure;
    int cellId;

    virtual void render()=0;
    void init(float mass, QVector3D pos, rp3d::DynamicsWorld* worldRef);
    QVector3D getPosition();
    QVector3D getVelocity();
    QQuaternion getOrientation();

    void applyForce(QVector3D force);

    void makeRigid();
    void makeFluid();

    static constexpr float PHY_WORLD_SCALE = 1.0f/10.0f;
    static const uint CAT_RIGID = 0x0001;
    static const uint CAT_FLUID = 0x0002;
};

#endif // SYSPARTICLE_H
