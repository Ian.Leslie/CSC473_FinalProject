#ifndef GLUTILITY_H
#define GLUTILITY_H
#include <QVector3D>
#include <QOpenGLFunctions>

class GLUtility
{
public:
    static void setPositionAndOrientation(QVector3D pos, QQuaternion orient);

    static void renderBox(float scale);
    static void renderBox(float xScale, float yScale, float zScale);

    static void renderSphere(float scale);
    static void renderSphere(float scale, bool drawLines);

    static void setNormalFromTriangle(QVector3D* v0,
                                      QVector3D* v1,
                                      QVector3D* v2);

    static void qVertexWrap(QVector3D* vect);
    static void qNormalWrap(QVector3D* vect);
};

#endif // GLUTILITY_H
