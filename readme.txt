This is the source code repository for my final CSC473 project:
Adaptively Animating Groups of Rigid Bodies as a Fluid

Basically if you've got a whole bunch of rigid-body spheres interacting,
the spheres in the densely packed areas will adaptively be treated as
an SPH Fluid.

I've been running this project with Qt Creator 4.4.1

NOTE:
	I had trouble getting the ReactPhysics3D library to compile
	using its make-file and ended up just sticking all the source
	files into the project.
	It's a pretty ugly solution and causes a 100+ warnings to be
	generated every time I compile the project, but since it works
	I haven't bothered to change it since.


Important Files:
	glwidget.cpp
		Contains all the SPH fluid calculations
		and general timestep cycle

		Also contains scoring function code

	proximitymanager.cpp
		Contains all the code for constructing a KD-tree
		and running queries on it

	kernelf.cpp
		Contains the code for kernel function that
		the SPH simulation uses
		(I also re-use it for the scoring function)

	sysparticle.cpp
		This is the generic object used in simulations that
		can be either rigid or fluid.
		This is mainly just a wrapper for a ReactPhysics3D body
		with a couple fluid-specific properties added

The rest of the files can largely be ignored.
Like I mention above, a lot of them are part of the ReactPhysics3D engine,
and the rest mainly deal with getting the OpenGL stuff setup with a movable
camera and rendering the objects.