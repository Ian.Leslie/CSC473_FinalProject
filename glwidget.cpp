#include "glwidget.h"
#include <QtMath>
#include <QEvent>
#include <QDebug>
#include <QKeyEvent>
#include <glutility.h>
#include <QMatrix4x4>
#include <fstream>
#include "syssphere.h"
#include <random>
#include <QElapsedTimer>

/* Subclass QGLWidget used to access the OpenGL API
 *
 * This class handles pretty much all of the high-level logic
 * associated with this assignment.
 */
GLWidget::GLWidget(QWidget *parent) :
    QGLWidget(parent)
{
    camera = glcamera(180.0f,
                       0.0f,
                       6.0f,
                       0.2f,
                       180.0f);
    _keyCheckTimer = new QTimer(this);
    connect(_keyCheckTimer,SIGNAL(timeout()), this, SLOT(updateCamera()));
    _keyCheckTimer->start(30);
    _animStepTimer = new QTimer(this);
    connect(_animStepTimer,SIGNAL(timeout()), this, SLOT(animStep()));
    _animStepTimer->start(30);
    _animPaused = true;

    _partList = QList<SysParticle*>();
    _dynWorld = NULL;
    _proxManager = NULL;
    _fpsAverager = new FpsAverager(10);
    _printTime = false;

    _proxManagerAll = NULL;
}

void GLWidget::setUpScene1()
{
    setUpGeneralScene(10,10,10,-1.0f);
}

void GLWidget::setUpScene2()
{
    setUpGeneralScene(10,10,10,2.0f);
}

void GLWidget::setUpScene3()
{
    setUpGeneralScene(10,10,10,0.8f);
}

void GLWidget::setUpGeneralScene(int iMax, int jMax, int kMax, float fractFluid)
{
    reset();

    _dynWorld = new rp3d::DynamicsWorld(rp3d::Vector3(0,-9.8,0));
    _floor = _dynWorld->createRigidBody(rp3d::Transform());
    _floor->setType(rp3d::STATIC);
    float floorRad = 1000;

    rp3d::ProxyShape *pShape = _floor->addCollisionShape(new rp3d::BoxShape(rp3d::Vector3(floorRad,floorRad,floorRad)),
                                            rp3d::Transform(rp3d::Vector3(0,-floorRad,0),rp3d::Quaternion::identity()),1.0);
    pShape->setCollisionCategoryBits(SysParticle::CAT_RIGID);
    pShape->setCollideWithMaskBits(SysParticle::CAT_RIGID | SysParticle::CAT_FLUID);


    float sphereRad = 0.1f;
    float wiggleRoom = 0.01f;
    float spacing = (sphereRad+wiggleRoom)*2.0 + 0.005f;
    QVector3D iV = QVector3D(0,1.0f,0);
    QVector3D jV = QVector3D(1.0f,0,0);
    QVector3D kV = QVector3D(0,0,1.0f);
    QVector3D startP = QVector3D(0.0,0.5f,0.0) - 0.5f*(float(jMax)*jV + float(kMax)*kV)*spacing;
    QVector3D pos;
    SysSphere* objRef;

    _proxManagerAll = new ProximityManager(true);
    _scoreKFun = KernelF(sphereRad*2.5f);
    _updateScoreEvery = 6; // Only update the object state every few cycles
    _updateScoreIn = 0;

    _proxManager = new ProximityManager(false);
    _kFun = KernelF(sphereRad*1.9f);
    _fluidStiffness = 4.0f;
    _fluidBaseDensity = 0.0f;
    _fluidViscosity = 0.05f;

    for(int i=0; i < iMax; i++){
        for(int j=0; j < jMax; j++){
            for(int k=0; k < kMax; k++){
                pos = startP + iV*float(i)*(spacing + getRandFloat(-wiggleRoom,wiggleRoom))\
                         + jV*float(j)*(spacing + getRandFloat(-wiggleRoom,wiggleRoom))\
                         + kV*float(k)*(spacing + getRandFloat(-wiggleRoom,wiggleRoom));
                objRef = new SysSphere(sphereRad,1.0f,pos,_dynWorld);
                if(getRandFloat(0,1.0f) < fractFluid){
                    objRef->makeFluid();
                }
                _proxManager->addObj(objRef);
                _proxManagerAll->addObj(objRef);
                _partList.append(objRef);
            }
        }
    }

    update();
}

void GLWidget::updateObjectStates(float threshold)
{
    _proxManagerAll->update();

    float scoreSum = 0.0f;

    for(int i=0; i<_partList.length(); i++){
        QVector3D pos = _partList[i]->getPosition();
        QVector3D vel = _partList[i]->getVelocity();
        QList<SysParticle*> closeList = _proxManagerAll->getObjsCloseTo(pos,_scoreKFun.getSupportRad());
        float baseScore = 0.0f;
        float velScore = 0.0f;
        for(int j=0; j<closeList.length(); j++){
            float velDiff = (vel - closeList[j]->getVelocity()).length();
            float s = _scoreKFun.getVal(pos - closeList[j]->getPosition());
            baseScore += velDiff*s;
            //velScore += (vel - closeList[j]->getVelocity()).length()*_scoreKFun.getVal(pos - closeList[j]->getPosition());
        }
        float modScore = baseScore - .5f;
        modScore = 1.0f/(1.0f + qExp(-0.6f*modScore));


        if(modScore > threshold && _partList[i]->isRigid){
            _partList[i]->makeFluid();
        }
        else if(modScore <= threshold && !_partList[i]->isRigid){
            _partList[i]->makeRigid();
        }
        scoreSum += modScore;
    }

}

void GLWidget::reset(){
    if(_dynWorld == NULL){
        //Nothing to reset
        return;
    }

    delete _dynWorld;
    _dynWorld = NULL;
    if(_proxManager != NULL){
        delete _proxManager;
        _proxManager = NULL;
    }
    if(_proxManagerAll != NULL){
        delete _proxManagerAll;
        _proxManagerAll = NULL;
    }
    _partList.clear();

    update();
}

float GLWidget::getRandFloat(float min, float max){
    return min + (max-min)*float(std::rand())/float(RAND_MAX);
}

void GLWidget::reportTime(char *str, qint64 elapsed)
{
    if(_printTime)
        qDebug() << str << " " << (elapsed-_lastTimeReport) << "(Total: " << elapsed << ")";
    _lastTimeReport = elapsed;
}

void GLWidget::animStep()
{
    if(_animPaused || _dynWorld==NULL){
        return;
    }
    float dt = 1.0f/30.0f;
    QElapsedTimer updateTimer;
    updateTimer.start();
    _lastTimeReport = 0;
    if(_printTime)
        qDebug() << "------------------------------";

    if(dynamicCB->isChecked()){
        _updateScoreIn--;
        if(_updateScoreIn < 0){
            _updateScoreIn = _updateScoreEvery;
            float thresholdV = float(thresholdSlider->value()) / float(thresholdSlider->maximum());
            updateObjectStates(thresholdV);
        }
    }

    _proxManager->update();

    reportTime("Proxy Update                  ", updateTimer.elapsed());

    float supportRad = _kFun.getSupportRad();

    QList<SysParticle*> *closeListArr = new QList<SysParticle*>[_partList.length()];
    for(int i=0; i<_partList.length(); i++){
        SysParticle* partRef = _partList[i];
        if(partRef->isRigid) continue;
        closeListArr[i] = _proxManager->getObjsCloseTo(partRef->getPosition(),supportRad);
    }

    reportTime("List Built                    ", updateTimer.elapsed());

    for(int i=0; i<_partList.length(); i++){
        SysParticle* partRef = _partList[i];
        if(partRef->isRigid) continue;

        // Calculate density
        QList<SysParticle*> closePartList = closeListArr[i];
        partRef->density = 0.0f;
        for(int j=0; j<closePartList.length(); j++){
            SysParticle* closePart = closePartList[j];
            partRef->density += closePart->mass*_kFun.getVal(closePart->getPosition() - partRef->getPosition());
        }

        // Calculate preassure
        partRef->preassure = _fluidStiffness*(partRef->density - _fluidBaseDensity);
    }

    reportTime("Preassure & Density Calculated", updateTimer.elapsed());
    //qint64 elapsedMilliMid = updateTimer.elapsed();

    for(int i=0; i<_partList.length(); i++){
        SysParticle* mObj = _partList[i];
        if(mObj->isRigid) continue;

        // acelV = -pGradTerm + _fluidViscosity*lVelTerm

        QVector3D pGradTerm = QVector3D();
        QVector3D lVelTerm  = QVector3D();
        QList<SysParticle*> closePartList = closeListArr[i];
        for(int j=0; j<closePartList.length(); j++){
            SysParticle* oObj = closePartList[j];

            pGradTerm += oObj->mass*(  mObj->preassure/(mObj->density*mObj->density)
                                     + oObj->preassure/(oObj->density*oObj->density))
                    * _kFun.getGradient(mObj->getPosition() - oObj->getPosition());

            lVelTerm += oObj->mass*( (oObj->getVelocity() - mObj->getVelocity())/mObj->density )
                    * _kFun.getLaplacian(mObj->getPosition() - oObj->getPosition() );

        }

        QVector3D acel = -pGradTerm + _fluidViscosity*lVelTerm;
        mObj->applyForce(acel*mObj->mass);
    }

    reportTime("Forces calculated & applied   ", updateTimer.elapsed());

    delete closeListArr;

    _dynWorld->update(dt);

    reportTime("Physics update done           ", updateTimer.elapsed());

    qint64 elapsedMilli = updateTimer.elapsed();

    //qDebug() << elapsedMilliMid << ", " << (elapsedMilli-elapsedMilliMid);

    float fps = 1000.0f / float(elapsedMilli);
    _fpsAverager->reportValue(fps);

    fpsLabel->setText(QString("%1 FPS").arg(int(_fpsAverager->getValue())));

    int fluidCount = 0;
    for(int i=0; i<_partList.length(); i++){
        if(!_partList[i]->isRigid){
            fluidCount += 1;
        }
    }
    rigidLabel->setText(QString("%1 Rigid Objects").arg(_partList.length()-fluidCount));
    fluidLabel->setText(QString("%1 Fluid Objects").arg(fluidCount));

    update();
}

void GLWidget::drawAnimObjects()
{
    for(int i=0; i<_partList.length(); i++){
        if(_partList[i]->isRigid || !colorCodeCB->isChecked()){
            glColor3f(0.8,0.5,0.5);
        }
        else{
            glColor3f(0.5,0.5,0.8);
        }
        _partList[i]->render();
    }

    // Draw floor
    /*
    rp3d::Vector3 pos = _floor->getTransform().getPosition();
    glPushMatrix();
    glTranslatef(pos.x,pos.y - 0.1f,pos.z);
    glColor3f(0.8,0.7,0.8);
    GLUtility::renderBox(20,0.2,20);
    glPopMatrix();
    */
}

bool GLWidget::isKeyDown(int keyCode)
{
    if (_keyDownState.contains(keyCode)){
        return _keyDownState[keyCode];
    }
    return false;
}

void GLWidget::updateCamera()
{
    float yawIncr = 3.0f;
    float pitchIncr = 1.7f;
    float distIncr = 0.3f;

    bool dirty = false;
    if(isKeyDown(87)){
        //W
        camera.pitch += pitchIncr;
        dirty = true;
    }
    if(isKeyDown(83)){
        //S
        camera.pitch -= pitchIncr;
        dirty = true;
    }
    if(isKeyDown(65)){
        //A
        camera.yaw -= yawIncr;
        dirty = true;
    }
    if(isKeyDown(68)){
        //D
        camera.yaw += yawIncr;
        dirty = true;
    }
    if(isKeyDown(81)){
        //Q
        camera.dist -= distIncr;
        dirty = true;
    }
    if(isKeyDown(69)){
        //E
        camera.dist += distIncr;
        dirty = true;
    }
    if(dirty){
        //Update widget
        update();
    }
}

bool GLWidget::eventFilter(QObject *, QEvent *e){
    if ( e->type() == QEvent::KeyPress ) {
        QKeyEvent *k = static_cast<QKeyEvent*>(e);
        _keyDownState[k->key()] = true;
    }
    else if(e->type() == QEvent::KeyRelease){
        QKeyEvent *k = static_cast<QKeyEvent*>(e);
        _keyDownState[k->key()] = false;
    }
    return false;
}


void GLWidget::pauseTimer()
{
    _animPaused = true;
}

void GLWidget::resumeTimer()
{
    _animPaused = false;
}

void GLWidget::initializeGL(){
    glClearColor(0.6,0.6,0.6,1.0);
}

void GLWidget::paintGL(){
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_COLOR_MATERIAL);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    camera.applyModelTransform();

    if(true){
        glDisable(GL_LIGHTING);
        glBegin(GL_LINES);
        // X-axis
        glColor3f(1,0,0);
        glVertex3f(0,0,0);
        glVertex3f(2,0,0);
        // Y-axis
        glColor3f(0,1,0);
        glVertex3f(0,0,0);
        glVertex3f(0,2,0);
        // Z-axis
        glColor3f(0,0,1);
        glVertex3f(0,0,0);
        glVertex3f(0,0,2);
        glEnd();
        glEnable(GL_LIGHTING);
    }

    drawAnimObjects();
}

void GLWidget::resizeGL(int w, int h){
    glViewport(0,0,w,h);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    float ratio = float(w)/float(h);
    float fov = M_PI/8.0f;
    float n = 0.1f;
    float f = 100.0f;
    float nearPlaneRad = n*qTan(fov);
    glFrustum(nearPlaneRad*ratio,
              -nearPlaneRad*ratio,
              -nearPlaneRad,
              nearPlaneRad,
              n,
              f);
}
