#include "sysparticle.h"

void SysParticle::init(float mass, QVector3D pos, reactphysics3d::DynamicsWorld* worldRef)
{
    rp3d::Transform trans = rp3d::Transform(rp3d::Vector3(pos.x(),pos.y(),pos.z())/SysParticle::PHY_WORLD_SCALE,
                                            rp3d::Quaternion::identity());
    bodyRef = worldRef->createRigidBody(trans);
    this->mass = mass;
    cellId = -1;
}

QVector3D SysParticle::getPosition()
{
    rp3d::Vector3 pos = bodyRef->getTransform().getPosition();
    return QVector3D(pos.x,pos.y,pos.z)*SysParticle::PHY_WORLD_SCALE;
}

QVector3D SysParticle::getVelocity()
{
    rp3d::Vector3 vel = bodyRef->getLinearVelocity();
    return QVector3D(vel.x,vel.y,vel.z)*SysParticle::PHY_WORLD_SCALE;
}

QQuaternion SysParticle::getOrientation()
{
    rp3d::Quaternion orient = bodyRef->getTransform().getOrientation();
    return QQuaternion(orient.w,orient.x,orient.y,orient.z);
}

void SysParticle::applyForce(QVector3D force)
{
    rp3d::Vector3 scaledForce = rp3d::Vector3(force.x(),force.y(),force.z())/SysParticle::PHY_WORLD_SCALE;
    bodyRef->applyForceToCenterOfMass(scaledForce);
}

void SysParticle::makeRigid()
{
    isRigid = true;
    rp3d::ProxyShape *pShape = &bodyRef->getProxyShapesList()[0];
    pShape->setCollisionCategoryBits(SysParticle::CAT_RIGID);
    pShape->setCollideWithMaskBits(SysParticle::CAT_RIGID | SysParticle::CAT_FLUID);
}

void SysParticle::makeFluid()
{
    isRigid = false;
    rp3d::ProxyShape *pShape = &bodyRef->getProxyShapesList()[0];
    pShape->setCollisionCategoryBits(SysParticle::CAT_FLUID);
    pShape->setCollideWithMaskBits(SysParticle::CAT_RIGID);
}
