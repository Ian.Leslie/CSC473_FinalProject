#include "kdnode.h"

KDNode::KDNode(int startIdx, int endIdx)
{
    this->startIdx = startIdx;
    this->endIdx = endIdx;
    this->splitValue = 0;
    this->splitDim = -1;

    this->leftChild = nullptr;
    this->rightChild = nullptr;
}

KDNode::~KDNode()
{
    if(leftChild != nullptr){
        delete leftChild;
    }
    if(rightChild != nullptr){
        delete rightChild;
    }
}
