#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->renderWidget->fpsLabel = ui->fpsLabel;
    ui->renderWidget->thresholdSlider = ui->thresholdSlider;
    ui->renderWidget->dynamicCB = ui->dynamicCB;
    ui->renderWidget->colorCodeCB = ui->coloredCB;

    ui->renderWidget->rigidLabel = ui->rigidLabel;
    ui->renderWidget->fluidLabel = ui->fluidLabel;
    this->installEventFilter(ui->renderWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}
