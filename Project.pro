#-------------------------------------------------
#
# Project created by QtCreator 2017-09-20T16:39:58
#
#-------------------------------------------------

QT       += core gui opengl

LIBS += -LI:\Qt\5.9.3\mingw53_32\lib\libQt5OpenGL.a -lopengl32

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Project
TEMPLATE = app


FORMS    += mainwindow.ui

HEADERS += \
    body/Body.h \
    body/CollisionBody.h \
    body/RigidBody.h \
    collision/broadphase/BroadPhaseAlgorithm.h \
    collision/broadphase/DynamicAABBTree.h \
    collision/narrowphase/EPA/EdgeEPA.h \
    collision/narrowphase/EPA/EPAAlgorithm.h \
    collision/narrowphase/EPA/TriangleEPA.h \
    collision/narrowphase/EPA/TrianglesStore.h \
    collision/narrowphase/GJK/GJKAlgorithm.h \
    collision/narrowphase/GJK/Simplex.h \
    collision/narrowphase/CollisionDispatch.h \
    collision/narrowphase/ConcaveVsConvexAlgorithm.h \
    collision/narrowphase/DefaultCollisionDispatch.h \
    collision/narrowphase/NarrowPhaseAlgorithm.h \
    collision/narrowphase/SphereVsSphereAlgorithm.h \
    collision/shapes/AABB.h \
    collision/shapes/BoxShape.h \
    collision/shapes/CapsuleShape.h \
    collision/shapes/CollisionShape.h \
    collision/shapes/ConcaveMeshShape.h \
    collision/shapes/ConcaveShape.h \
    collision/shapes/ConeShape.h \
    collision/shapes/ConvexMeshShape.h \
    collision/shapes/ConvexShape.h \
    collision/shapes/CylinderShape.h \
    collision/shapes/HeightFieldShape.h \
    collision/shapes/SphereShape.h \
    collision/shapes/TriangleShape.h \
    collision/CollisionDetection.h \
    collision/CollisionShapeInfo.h \
    collision/ContactManifold.h \
    collision/ContactManifoldSet.h \
    collision/ProxyShape.h \
    collision/RaycastInfo.h \
    collision/TriangleMesh.h \
    collision/TriangleVertexArray.h \
    constraint/BallAndSocketJoint.h \
    constraint/ContactPoint.h \
    constraint/FixedJoint.h \
    constraint/HingeJoint.h \
    constraint/Joint.h \
    constraint/SliderJoint.h \
    engine/CollisionWorld.h \
    engine/ConstraintSolver.h \
    engine/ContactSolver.h \
    engine/DynamicsWorld.h \
    engine/EventListener.h \
    engine/Impulse.h \
    engine/Island.h \
    engine/Material.h \
    engine/OverlappingPair.h \
    engine/Profiler.h \
    engine/Timer.h \
    mathematics/mathematics.h \
    mathematics/mathematics_functions.h \
    mathematics/Matrix2x2.h \
    mathematics/Matrix3x3.h \
    mathematics/Quaternion.h \
    mathematics/Ray.h \
    mathematics/Transform.h \
    mathematics/Vector2.h \
    mathematics/Vector3.h \
    memory/MemoryAllocator.h \
    memory/Stack.h \
    configuration.h \
    decimal.h \
    glcamera.h \
    glutility.h \
    glwidget.h \
    mainwindow.h \
    reactphysics3d.h \
    sysparticle.h \
    syssphere.h \
    proximitymanager.h \
    kernelf.h \
    kdnode.h \
    fpsaverager.h

SOURCES += \
    body/Body.cpp \
    body/CollisionBody.cpp \
    body/RigidBody.cpp \
    collision/broadphase/BroadPhaseAlgorithm.cpp \
    collision/broadphase/DynamicAABBTree.cpp \
    collision/narrowphase/EPA/EdgeEPA.cpp \
    collision/narrowphase/EPA/EPAAlgorithm.cpp \
    collision/narrowphase/EPA/TriangleEPA.cpp \
    collision/narrowphase/EPA/TrianglesStore.cpp \
    collision/narrowphase/GJK/GJKAlgorithm.cpp \
    collision/narrowphase/GJK/Simplex.cpp \
    collision/narrowphase/ConcaveVsConvexAlgorithm.cpp \
    collision/narrowphase/DefaultCollisionDispatch.cpp \
    collision/narrowphase/NarrowPhaseAlgorithm.cpp \
    collision/narrowphase/SphereVsSphereAlgorithm.cpp \
    collision/shapes/AABB.cpp \
    collision/shapes/BoxShape.cpp \
    collision/shapes/CapsuleShape.cpp \
    collision/shapes/CollisionShape.cpp \
    collision/shapes/ConcaveMeshShape.cpp \
    collision/shapes/ConcaveShape.cpp \
    collision/shapes/ConeShape.cpp \
    collision/shapes/ConvexMeshShape.cpp \
    collision/shapes/ConvexShape.cpp \
    collision/shapes/CylinderShape.cpp \
    collision/shapes/HeightFieldShape.cpp \
    collision/shapes/SphereShape.cpp \
    collision/shapes/TriangleShape.cpp \
    collision/CollisionDetection.cpp \
    collision/ContactManifold.cpp \
    collision/ContactManifoldSet.cpp \
    collision/ProxyShape.cpp \
    collision/RaycastInfo.cpp \
    collision/TriangleMesh.cpp \
    collision/TriangleVertexArray.cpp \
    constraint/BallAndSocketJoint.cpp \
    constraint/ContactPoint.cpp \
    constraint/FixedJoint.cpp \
    constraint/HingeJoint.cpp \
    constraint/Joint.cpp \
    constraint/SliderJoint.cpp \
    engine/CollisionWorld.cpp \
    engine/ConstraintSolver.cpp \
    engine/ContactSolver.cpp \
    engine/DynamicsWorld.cpp \
    engine/Island.cpp \
    engine/Material.cpp \
    engine/OverlappingPair.cpp \
    engine/Profiler.cpp \
    engine/Timer.cpp \
    mathematics/mathematics_functions.cpp \
    mathematics/Matrix2x2.cpp \
    mathematics/Matrix3x3.cpp \
    mathematics/Quaternion.cpp \
    mathematics/Transform.cpp \
    mathematics/Vector2.cpp \
    mathematics/Vector3.cpp \
    memory/MemoryAllocator.cpp \
    glcamera.cpp \
    glutility.cpp \
    glwidget.cpp \
    main.cpp \
    mainwindow.cpp \
    sysparticle.cpp \
    syssphere.cpp \
    proximitymanager.cpp \
    kernelf.cpp \
    kdnode.cpp \
    fpsaverager.cpp

