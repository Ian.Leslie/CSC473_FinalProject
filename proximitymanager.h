#ifndef PROXIMITYMANAGER_H
#define PROXIMITYMANAGER_H

#include "sysparticle.h"
#include <QList>
#include "kdnode.h"

class ProximityManager
{
public:
    ProximityManager();
    ProximityManager(bool bothTypes);
    ~ProximityManager();

    void addObj(SysParticle* objRef);
    void update();
    QList<SysParticle*> getObjsCloseTo(QVector3D pos, float dist);
    QList<SysParticle*> getObjsInZone(QVector3D minVals, QVector3D maxVals);
    int getActiveCount();

private:
    QList<SysParticle*> _activeList;
    QList<SysParticle*> _inactiveList;
    SysParticle** _tempList;
    int _smallestGroup;
    bool _bothTypes;

    KDNode* _kdTree;

    KDNode* buildKDFor(int firstIdx, int lastIdx, int dimIdx);
    void sortRegion(int firstIdx, int lastIdx, int dimIdx);
    inline void mergedSortedRegions(int firstIdx, int windowSize, int maxIdx, int dimIdx);
    inline bool _isLT(int i, int j, int dimIdx);
    inline float _getV(int i, int dimIdx);

    void _internalGetObjsInZone(KDNode* node, QVector3D minVals, QVector3D maxVals, QList<SysParticle*>* outList);

};

#endif // PROXIMITYMANAGER_H
