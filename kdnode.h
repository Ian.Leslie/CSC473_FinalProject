#ifndef KDNODE_H
#define KDNODE_H


class KDNode
{
public:
    KDNode(int startIdx, int endIdx);
    ~KDNode();

    float splitValue;
    int splitDim;
    int startIdx;
    int endIdx;

    KDNode* leftChild;
    KDNode* rightChild;
};

#endif // KDNODE_H
