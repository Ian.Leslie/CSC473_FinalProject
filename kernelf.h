#ifndef KERNELF_H
#define KERNELF_H

#include <QVector3D>

class KernelF
{
public:
    KernelF();
    KernelF(float supportRad);

    float getSupportRad();
    float getVal(QVector3D v);
    QVector3D getGradient(QVector3D v);
    float getLaplacian(QVector3D v);

private:
    float _s;

    float getGradientPart(float vLen);

};

#endif // KERNELF_H
